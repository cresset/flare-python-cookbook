# Flare Python Cookbook

Welcome to [Flare Python Cookbook](https://www.cresset-group.com/support/support-resources/flare-python-cookbook/) repository. This repository contains a Jupyter Notebook with one-task ready-to-use recipes to help you automate and customize your tasks using Flare Python API.

The recipes in the Flare Python Cookbook are divided into categories:
* Getting Started
* Proteins
* Ligands
* Docking 

The functionalities illustrated in the Cookbook are:
* Read and write ligand and protein files into and from Flare
* Protein preparation and chain manipulation
* Select ligands and proteins from Ligands and Proteins tables
* Read property values and add columns from/to the Ligands table
* Run a docking experiment

Updates to the recipes may be found at the [flare-python-cookbook repository](https://gitlab.com/cresset/flare-python-cookbook).

We recommend using the Flare Python Cookbook with Flare 7.0. If you do not have Flare 7.0, please contact us at <enquiries@cresset-group.com> to request a free evaluation. The installation instructions for Flare can be found [here](https://www.cresset-group.com/support/installation/).

### Downloading the Cookbook

The easiest way to get the Flare Python Cookbook is to download the whole repository by clicking the download button (which looks like a tray with a down arrow) and selecting 'Download zip'. 

### Setting up the Cookbook

Extract the downloaded zip file, and load the Jupyter Notebook, 'File Python Cookbook.ipynb' file, into the [Flare Python Notebook](https://www.cresset-group.com/support/support-resources/python-extensions-flare/) extension. You can also see the content of the Jupyter Notebook via the 'File Python Cookbook.html' file.

Instructions for setting up and testing the Flare Python Cookbook can be found at the [Flare Python Cookbook](https://www.cresset-group.com/support/support-resources/flare-python-cookbook/) webpage.
